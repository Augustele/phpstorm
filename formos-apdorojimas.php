<?php
include_once  'help.php';
include_once 'classes/Person.php';

// sukurti nauja Person objekta
$person = new Person(); //cia is kintamojo padaro nauja objekta (klase)

// sudeti duomenis is POST i objekta naudojant setterius
$person->setAge($_POST["age"]);
$person->setGender($_POST["gender"]);
$person->setName($_POST["name"]);
$person->setSurname($_POST["surname"]);
$person->setBirthdate($_POST["birthdate"]);
$person->setEmail($_POST["email"]);

// panaudojant dd f-ja atvaizduoji person objekta su sudetais duomenim

dd($person);

