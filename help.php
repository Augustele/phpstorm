<?php

function dbconnect()
{
    $connection = null;
    try{
        $connection = new PDO('mysql:host=localhost; dbname=classicmodels', 'root');
    } catch (Exception $e) {
        dd($e);
    }

    return $connection; //return nurodo kad grazinti sita kintamaji.
}

function dd($what)
{
    echo '<pre>';
    print_r($what);
    die();
}


function convertDuration($minutes)
{
    echo gmdate("H:i, $minutes * 60");
}


